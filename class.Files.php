<?php
class Files{
    private $fileReg='';
    private $error='';
    private $maxSize=0;
    public function __construct($maxSizeKB=null) {
        $this->maxSize=  is_null($maxSizeKB)?0:$maxSizeKB;
        $this->fileReg='/^([a-zа-я0-9_\,\[\]( -]{1,100}).(zip|7z|rar|gz|pdf|doc|docx|xls|xlsx|jpg|gif|png|jpeg){1}$/ui';
    }
    public function getErrorInfo()
    {
        return $this->error;
    }
    public function copyPost($arrFile,$folder=null,$prefix=null)
    {
        if($arrFile['tmp_name']!="")
        {
            
            if($arrFile['size']>($this->maxSize*1024))
            {
                $this->error="Размер файла превышает ".$this->maxSize." Мб.";
                return false;
            }
            else
            {
                if(!preg_match($this->fileReg,$arrFile['name']))
                {
                    $this->error='Неверный тип или имя файла';
                    return false;
                }
                else
                {                                            
                    $fpath=$arrFile['name'];
                    if(!is_null($prefix))
                        $fpath=$prefix.'-'.$fpath;
                    $fpath=str_replace(' ', '_', $fpath);
                    if(preg_match('/([а-яА-Я])+/u', $fpath))
                    {
                        $fpath=SystemData::strTranslit($fpath);                            
                    }
                    if(file_exists($folder.$fpath))
                    {
                        $j=1;
                        while(1)
                        {
                            if($j==1)
                            { 
                                $arr=array();
                                preg_match($this->fileReg, $fpath,$arr);
                                $fpath=$arr[1]."($j).".$arr[2];
                            }
                            else {
                                $fpath= preg_replace('/([0-9)(]{1,8}){1}/',"($j)", $fpath);
                            }

                            if(!file_exists($folder.$fpath))
                                break;
                            else {
                                $j++;
                            }
                        }
                    }
                    if(!move_uploaded_file($arrFile['tmp_name'],$folder.$fpath))
                    {
                        $this->error="Ошибка копирования";
                        return false;
                    }
                    return $folder.$fpath;
                }                            
            }
        }
        else
        {
            $this->error="Переданн некорректный массив";
            return false;
        }
    }
    public function deleteFile($filePath)
    {
        return unlink($filePath);
    }
    public function readDir($path,$type="all",$filter=null)
    {
        $arr=array();
        $dh = opendir ($path);        
        while ($dir = readdir($dh)) {       
            if (($type=='dir' && is_dir($path . $dir)) || $type=="all" || ($type=="files" && !is_dir($path . $dir))) {         
                if(is_null($filter))
                    $arr[]=$dir;
                else
                {
                     if(preg_match($filter,$dir))
                             $arr[]=$dir;
                }
            }           
        } 
        closedir($dh);
        sort($arr);
        return $arr;
    }
    public function readFile($filePath)
    {
        if(file_exists($filePath))
        {
            $file=fopen($filePath,"r");
            if(filesize($filePath)==0)
                return null;
            $res=fread($file,filesize($filePath));
            fclose($file);
            return $res;
        }
        else 
        {
            $this->error="Файл не найден";
            return false;
        }
    }
    public function writeFile($filePath, $text)
    {
        $file=fopen($filePath,"w");
        fwrite($file, $text);
        fclose($file);
    }

}