<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Landing Page</title>
    <?
    Landing::getScripts();
    Landing::getStyles();
    ?>
</head>
<body>
<?=Landing::getPanel()?>
<?=Landing::drawBlock("top")?>
<?=Landing::drawBlock("middle")?>
<?=Landing::drawBlock("bottom")?>
</body>
</html>