<?php
require_once "class.Template.php";
require_once "class.Files.php";
class Landing{
    CONST password="land@02";
    public static function drawBlock($name){
        $tpl=new Template($name);
        if(self::checkRule())
            return "<div class=\"landing-block block-$name\"><div title=\"Редактировать код\" data-rel=\"$name\" class=\"landing-block-control\"></div>".$tpl->getHtml()."</div>";
        else
            return $tpl->getHtml();

    }
    public static function getScripts(){
        print '
                <script src="scripts/jquery.js"></script>
                <script src="scripts/uk/js/uikit.js"></script>
                <script src="scripts/core.js"></script>
        ';
        if(self::checkRule()) {
            print '
                <link rel="stylesheet" href="scripts/codemirror/lib/codemirror.css">
                <script src="scripts/codemirror/lib/codemirror.js"></script>
                <script src="scripts/codemirror/mode/markdown/markdown.js"></script>
                <script src="scripts/codemirror/addon/mode/overlay.js"></script>
                <script src="scripts/codemirror/mode/xml/xml.js"></script>
                <script src="scripts/codemirror/mode/gfm/gfm.js"></script>
                <script src="scripts/marked-master/marked.min.js"></script>
                <link rel="stylesheet" href="scripts/uk/css/components/htmleditor.css">
                <script src="scripts/uk/js/components/htmleditor.js"></script>
                ';
        }
    }
    public static function getStyles(){
        print '
                <link href="scripts/uk/css/uikit.css" rel="stylesheet"/>
                <link href="css/core.css" rel="stylesheet"/>
                <link href="css/template.css" rel="stylesheet"/>
        ';
    }
    public static function getPanel(){
        if(!self::checkRule())
            return null;
        $tpl=new Template('admin-panel');
        return $tpl->getHtml();
    }
    public static function checkRule(){
        if(isset($_REQUEST['password']))
        {
            if(self::password==isset($_REQUEST['password'])) {
                $_SESSION['isAdmin'] = true;
                header('Location:'.$_SERVER['DOCUMENT_ROOT']);
            }
        }
        return isset($_SESSION['isAdmin'])?true:false;
    }
    public static function getFileContent($name){

        if(!self::checkRule())
            return null;
        $f=new Files();

        if($name=='main-template')
            return "<textarea>".$f->readFile('template.php')."</textarea>";
        elseif($name=='main-stylesheet')
            return "<textarea>".$f->readFile('css/template.css')."</textarea>";
        else
            return "<textarea>".$f->readFile('templates/'.$name.'.html')."</textarea>";

    }
    public static function setFileContent($name,$html){

        if(!self::checkRule())
            return null;
        $f=new Files();
        if($name=='main-template')
            $f->writeFile('template.php',$html);
        elseif($name=='main-stylesheet')
            $f->writeFile('css/template.css',$html);
        else
            $f->writeFile('templates/'.$name.'.html',$html);

    }
}
if(isset($_REQUEST['value']) && Landing::checkRule()) {
    if (preg_match('/^[a-z0-9_\-]{3,100}$/i', $_REQUEST['value'])) {
        if(isset($_REQUEST['html']))
        {
            Landing::setFileContent($_REQUEST['value'],$_REQUEST['html']);
            print $_REQUEST['value'];
        }
        else
            print Landing::getFileContent($_REQUEST['value']);
        exit;
    }
}