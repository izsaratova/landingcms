var htmleditor;
$(document).ready(function(){
    $('.landing-block-control').click(function () {
        $('#saveCode').attr('data-rel',$(this).attr('data-rel'));
        $('#codeTitle').html("/templates/"+$(this).attr('data-rel')+".html");
        $.get('/',{value:$(this).attr('data-rel')},function(data){
            $('#codeWrapper').html(data);

            htmleditor = UIkit.htmleditor($('#codeWrapper > textarea'), { mode:"tab" });
            var modal = UIkit.modal("#codeModal");
            modal.show();
        });
    });
    $('#saveCode').click(function(){
        $.post('/',{value:$('#saveCode').attr('data-rel'),html:htmleditor.currentvalue},function(data){
            if(data=="main-template" || data=="main-stylesheet")
                document.location="/";
            else {
                $('.landing-block.block-' + data).html(htmleditor.currentvalue);
                var modal = UIkit.modal("#codeModal");
                modal.hide();
            }
        });
    });
    $('.template-edit').click(function(){
        $('#saveCode').attr('data-rel','main-template');
        $('#codeTitle').html("/template.php");
        $.get('/',{value:'main-template'},function(data){
            $('#codeWrapper').html(data);
            htmleditor = UIkit.htmleditor($('#codeWrapper > textarea'), { mode:"tab" });
            var modal = UIkit.modal("#codeModal");
            modal.show();
        });
    });
    $('.styles-edit').click(function(){
        $('#saveCode').attr('data-rel','main-stylesheet');
        $('#codeTitle').html("/css/template.css");
        $.get('/',{value:'main-stylesheet'},function(data){
            $('#codeWrapper').html(data);
            htmleditor = UIkit.htmleditor($('#codeWrapper > textarea'), { mode:"tab" });
            var modal = UIkit.modal("#codeModal");
            modal.show();
        });
    });
    $('.edit-mode').click(function(){
        if($(this).attr('data-state')=='on') {
            $('.landing-block').addClass('landing-block-disabled');
            $('.landing-block').removeClass('landing-block');
            $(this).attr('data-state','off');
            $('.edit-mode-icon').removeClass('uk-icon-toggle-on');
            $('.edit-mode-icon').addClass('uk-icon-toggle-off');
            $('.landing-block-control').addClass('uk-hidden');
        }
        else
        {
            $('.landing-block-disabled').addClass('landing-block');
            $('.landing-block-disabled').removeClass('landing-block-disabled');
            $(this).attr('data-state','on');
            $('.edit-mode-icon').addClass('uk-icon-toggle-on');
            $('.edit-mode-icon').removeClass('uk-icon-toggle-off');
            $('.landing-block-control').removeClass('uk-hidden');
        }
    });
});