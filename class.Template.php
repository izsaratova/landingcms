<?php
class Template{
    private $tpl;
    private $result;
    private $showShema;
    private $path;
    function __construct($tpl_name)
    {
        $this->showShema=false;
        $path='templates/'.mb_strtolower($tpl_name).'.html';
        if(!file_exists($path))
        {
            $file=fopen($path,"w");
            fclose($file);
        }
        $this->path=$path;
        $file=fopen($path,"r");
        if(filesize($path)>0)
            $page=fread($file,filesize($path));
        else
            $page="- empty block [$tpl_name] -";
        fclose($file);
        $this->tpl=$page;
        $this->result=$page;
    }
    public function writeBlock($name,$value)
    {
        $tmp=$this->result;
        $this->result=str_replace('{'.strtoupper($name).'}',$value,$this->result);
        if($tmp==$this->result)
            return true;
        else
            return false;
    }
    public function writeText($blockName,$text)
    {
        return $this->writeBlock($blockName, htmlspecialchars($text));
    }
    public function writeHtml($blockName,$html)
    {
        return $this->writeBlock($blockName, $html);
    }
    public function clear()
    {
        $this->result=$this->tpl;
    }
    public function getHtml()
    {
        if($this->showShema)
            return '<div class="template-block" title="'.$this->path.'">'.$this->result.'</div>';
        else
            return $this->result;
    }
    public function getBlockArray($with_content=false)
    {
        if($with_content)
            preg_match_all('({[a-zA-Z0-9_-]{1,20}})',$this->result,$arr);
        else    
            preg_match_all('({[a-zA-Z0-9_-]{1,20}})',$this->tpl,$arr);
        return $arr[0];
    }
}
?>
